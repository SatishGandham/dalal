(function () {
    var app = angular.module('stockMarket', []);

    app.controller('graphController', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {

        $scope.stockData = [
            {'symbol': 'GOOG',
                'price': 0,
                'when': 1422255631334
            },
            {'symbol': 'FBOOK',
                'price': 0,
                'when': 1422255631334
            },
            {'symbol': 'RIL',
                'price': 0,
                'when': 1422255631334
            },
            {'symbol': 'APPL',
                'price': 0,
                'when': 1422255631334
            }

        ];

        // Look for new data only after the existing data is loaded
        // set to true when data no longer exists.
        start_interval = false


        // Load data recursively
        load_data = function (offset_url) {
            var url
            url = SITE_URL + offset_url;
            $http.get(url).success(function (data) {
                $scope.stockData = $scope.stockData.concat(data['objects']);

                if (data['meta']['next']) {
                    load_data(data['meta']['next'])
                } else if (data['objects'] && data['objects'].length) {
                    start_interval = true
                    last_state = data['objects'][data['objects'].length - 1]['id']
                }
            });
        }
        load_data('/api/state/?format=json')

        // Load the new data every 10 seconds
        // Last state is the last record loaded. We load from there.
        $interval(function () {

            if (!start_interval) {
                return
            }
            $http.get(SITE_URL+'/api/state/?format=json&id__gt=' + last_state).success(function (data) {
                $scope.stockData = $scope.stockData.concat(data['objects']);
                last_state = data['objects'][data['objects'].length - 1]['id']
            })

        }, 10000, 9999910);

    }]) //graphController

    app.directive('lineChart', function ($window) {
        return {
            restrict: 'EA',
            template: '<div id="graph"></div>',
            link: function (scope, elem) {
                var wrapper_width = $('#wrapper').width();
                // Set the dimensions of the canvas / graph
                margin = {top: 30, right: 20, bottom: 70, left: 30};
                width = wrapper_width - margin.left - margin.right;
                height = wrapper_width / 1.618 - margin.top - margin.bottom;

                var stockDataToPlot = scope.stockData;
                var x, y, xAxis, yAxis, graphFun;
                var d3 = $window.d3;
                var rawSvg = elem.find('svg');
                var svg = d3.select(rawSvg[0]);


                // Watch for changes in stock data, and redraw the graph.
                scope.$watchCollection('stockData', function (newVal, oldVal) {
                    stockDataToPlot = newVal;
                    stockDataToPlot = stockDataToPlot.slice(Math.max(stockDataToPlot.length - 5000, 1))
                    redrawLineChart();
                });

                // On window resize, remove the graph nad redraw it.
                $(window).resize(function () {
                    var wrapper_width = $('#wrapper').width();
                    width = wrapper_width - margin.left - margin.right;
                    height = wrapper_width / 1.618 - margin.top - margin.bottom;
                    $('#graph').html('');
                    drawLineChart();

                });

                // Pretty much D3 stuff.
                function setChartParameters() {
                    // Set the ranges
                    x = d3.time.scale().range([0, width]);
                    y = d3.scale.linear().range([height, 0]);

                    // Define the axes
                    xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(5);
                    yAxis = d3.svg.axis().scale(y).orient("left").ticks(5);

                    color = d3.scale.category10();   // set the colour scale

                    priceline = d3.svg.line()
                        .x(function (d) {
                               return x(d.when);
                           })
                        .y(function (d) {
                               return y(d.price);
                           });

                    graphFun = function (data) {
                        data.forEach(function (d) {
                            var format = d3.time.format("%Y-%m-%d");
                            if (typeof (d.when) !== 'number') {
                                d.when = Date.parse(d.when);

                            }
                            d.price = +d.price;

                        });

                        // Nest the entries by symbol
                        var dataNest = d3.nest()
                            .key(function (d) {
                                     return d.symbol;
                                 })
                            .entries(data);

                        // Scale the range of the data
                        x.domain(d3.extent(data, function (d) {
                            return d.when;
                        }));
                        y.domain([0, d3.max(data, function (d) {
                            return d.price;
                        })]);

                        var legendSpace = width / dataNest.length; // spacing for the legend

                        svg = d3.select("#graph").append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                        // Loop through each symbol / key
                        dataNest.forEach(function (d, i) {
                            svg.append("path")
                                .attr("class", "line ")
                                .style("stroke", function () { // Add the colours dynamically
                                           return d.color = color(d.key);
                                       })
                                .attr("id", 'tag' + d.key.replace(/\s+/g, '')) // assign ID
                                .attr("d", priceline(d.values));

                            // Add the Legend
                            svg.append("text")
                                .attr("x", (legendSpace / 2) + i * legendSpace)  // space legend
                                .attr("y", height + (margin.bottom / 2) + 5)
                                .attr("class", "legend " + d['key'])    // style the legend
                                .style("fill", function () { // Add the colours dynamically

                                           return d.color = color(d.key);
                                       })
                                .on("click", function () {
                                        // Determine if current line is visible
                                        var active = d.active ? false : true,
                                            newOpacity = active ? 0 : 1;
                                        // Hide or show the elements based on the ID
                                        d3.select("#tag" + d.key.replace(/\s+/g, ''))
                                            .transition().duration(100)
                                            .style("opacity", newOpacity);
                                        // Update whether or not the elements are active
                                        d.active = active;
                                    })
                                .text(d.key);
                        });

                    }

                    updateGraphFun = function (data) {
                        data.forEach(function (d) {
                            var format = d3.time.format("%Y-%m-%d");
                            if (typeof (d.when) !== 'number') {
                                d.when = Date.parse(d.when);

                            }
                            d.price = +d.price;
                        });


                        // Nest the entries by symbol
                        var dataNest = d3.nest()
                            .key(function (d) {
                                     return d.symbol;
                                 })
                            .entries(data);


                        // Scale the range of the data
                        x.domain(d3.extent(data, function (d) {
                            return d.when;
                        }));
                        y.domain([0, d3.max(data, function (d) {
                            return d.price;
                        })]);


                        var legendSpace = width / dataNest.length; // spacing for the legend

                        // Loop through each symbol / key
                        dataNest.forEach(function (d, i) {

                            svg.selectAll('#tag' + d.key.replace(/\s+/g, ''))
                                .attr("d", priceline(d.values));

                            // Add the Legend
                            svg.selectAll(".legend." + d['key'])
                                .attr("x", (legendSpace / 2) + i * legendSpace)  // space legend
                                .attr("y", height + (margin.bottom / 2) + 5)
                                .attr("class", "legend")    // style the legend
                                .style("fill", function () { // Add the colours dynamically
                                           return d.color = color(d.key);
                                       })
                                .on("click", function () {
                                        // Determine if current line is visible
                                        var active = d.active ? false : true,
                                            newOpacity = active ? 0 : 1;
                                        // Hide or show the elements based on the ID
                                        d3.select("#tag" + d.key.replace(/\s+/g, ''))
                                            .transition().duration(100)
                                            .style("opacity", newOpacity);
                                        // Update whether or not the elements are active
                                        d.active = active;
                                    })
                                .text(d.key);


                        });
                    }


                }

                function drawLineChart() {
                    setChartParameters();
                    graphFun(stockDataToPlot);
                    // Add the X Axis
                    svg.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    // Add the Y Axis
                    svg.append("g")
                        .attr("class", "y axis")
                        .attr("transform", "translate(0,0)")
                        .call(yAxis);
                }

                function redrawLineChart() {
                    setChartParameters();
                    updateGraphFun(stockDataToPlot)
                    svg.selectAll("g.y.axis").call(yAxis);
                    svg.selectAll("g.x.axis").call(xAxis);
                }
                drawLineChart()
            }
        }

    });


    // Provides data for the stocks table
    app.controller('stocksController', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {
        $http.get(SITE_URL+'/api/stock/?format=json').success(function (data) {
            $scope.stocks = data['objects']
        })

        $interval(function () {
            $http.get(SITE_URL+'/api/stock/?format=json').success(function (data) {
                $scope.stocks = data['objects']
            })
        }, 10000, 9999910);
    }])

})();
