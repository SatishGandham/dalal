from django.contrib import admin
from stocks.models import State, Stock

admin.site.register(State)
admin.site.register(Stock)
