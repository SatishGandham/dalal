from tastypie.resources import ModelResource
from stocks.models import Stock, State
from django.http import HttpResponse
import csv
import StringIO
from tastypie.serializers import Serializer


class CSVSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'csv']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'csv': 'text/csv',
    }

    def to_csv(self, data, options=None):
        options = options or {}
        data = self.to_simple(data, options)
        response = HttpResponse(content_type='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=data.csv'

        writer = csv.writer(response)
        for item in [{'SYMBOL': 'SYMBOL', 'date': 'date', 'price': 'price'}] + data['objects']:
            writer.writerow([unicode(item[key]).encode(
                "utf-8", "replace") for key in item.keys()])
        return response


    def from_csv(self, content):
        raw_data = StringIO.StringIO(content)
        data = []
        # Untested, so this might not work exactly right.
        for item in csv.DictReader(raw_data):
            data.append(item)
        return data


class StockResource(ModelResource):
    class Meta:
        queryset = Stock.objects.all()
        resource_name = 'stock'
        include_resource_uri = False
        fields = ['symbol', 'price', 'high', 'change', 'low']


class StateResource(ModelResource):
    class Meta:
        queryset = State.objects.all().order_by('id')
        resource_name = 'state'
        include_resource_uri = False
        fields = ['id', 'symbol', 'when', 'price']
        limit = 1000
        serializer = CSVSerializer()

        filtering = {
            'id': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }


    def dehydrate(self, bundle):
        # Include the request IP in the bundle.
        bundle.data['price'] = "%.2f" % bundle.data['price']
        return bundle

