from django.db import models
import random
from decimal import Decimal

def uniform_distribution(volatility):
    return random.uniform(-volatility, volatility)


def gaussian_distribution(volatility):
    return random.gauss(0.0, volatility/2.0)

class State(models.Model):
    symbol = models.CharField(max_length=6, db_index=True)
    when = models.DateTimeField()
    price = models.FloatField()

    def __str__(self):              # __unicode__ on Python 2
        return self.symbol +' '+ str(self.price)



class Stock(models.Model):
    name = models.CharField(max_length=12)
    symbol = models.CharField(primary_key=True, max_length=6, db_index=True)
    price = models.FloatField()
    distribution = models.CharField(max_length=12,
                                    default='uniform',
                                    choices=(
                                        ('uniform','Uniform distribution'),
                                        ('gauss','Gaussian distribution'),
                                    ))
    volatility = models.FloatField()
    change = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()

    distributions = {
        'uniform': uniform_distribution,
        'gauss': gaussian_distribution
    }

    def update_price(self, when, mo=.005,):
        oldPrice = self.price
        delta = self.distributions[self.distribution](self.volatility)
        self.price *= (1.0 + delta/2)
        self.price += mo*random.gauss(.1, .1)*self.change
        if self.price < 10:
            self.price = oldPrice
        self.change = self.price - (oldPrice)
        if(self.price < self.low):
            self.low = self.price
        if(self.price>self.high):
            self.high = self.price
        self.save()
        self.create_state(when)

    def __str__(self):              # __unicode__ on Python 2
        return self.name+' ('+self.distribution+', '+str(self.volatility)+')'

    def create_state(self, when):
        state = State()
        state.symbol = self.symbol
        state.price = self.price
        state.when = when
        state.save()

