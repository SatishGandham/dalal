from dalal.celery import app
from stocks.models import Stock
from django.utils import timezone

@app.task
def update_stock_prices():
    stocks = Stock.objects.all();
    when = timezone.now()
    for stock in stocks:
        stock.update_price(when)
        #print "Updated stock "+stock.name+" ,\t\t New price is:"+str(stock.price)


'''
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from stocks.models import Stock

@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
'''