from django.conf.urls import patterns, include, url
from stocks.api import StockResource, StateResource
from stocks import  views
stock_resource = StockResource()
state_resource = StateResource()

urlpatterns = patterns('',
    # The normal jazz here...
    url(r'^$', views.index),
    url(r'^api/', include(stock_resource.urls)),
    url(r'^api/', include(state_resource.urls)),
)