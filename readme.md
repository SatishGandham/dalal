# Dalal Street

Dalala Street (DS) is a stock market simulation with realtime feedback on the stock prices.
Uses dJango for the backend and angular and d3.js for the frontend.

###Simulation
Stock prices are generated based on preset volatility and distribution function (Gauss/uniform). Model also factors forward bias.
###Backend
A barebones dJango app which provides the api for the frontend using tastypie. Celery updates the stock prices every 10 seconds.
###Frontend
Angular fetches the data every 10 seconds, and updates the graph in realtime.

### Version
3.0.2

####To do
* Set stock prices to decimal with two places to save bandwidth. (update: Fixed it at the API level, would be better to fix at a lower level)
* Flush data beyond a point from the grpah, so that we don't overload the user browser with data. (update: Splicing the array to 10k elements)

## How it works
stocks/models.py describes two models.
* Stock
* State
 
Stock has a method called `update_price`. This method is called periodically by celery.
It updates the prices of all stocks, and then creates a state. State is stock price at that time.

== `update_price` takes argument `when`, which isn't actually necessary. In initial examples of D3 I explored, it was necessary for the d3 graph to have x axis points same for all stocks. Later realized it wasn't necessary, but left it.==

views.py delivers the index page, for angular to do its magic.
#####API
API code is located at `stocks/api.py`m
You can skip the `CSVSerializer` class, it's no longer used.

#####Celery
Celery task is defined at tasks.py

###Frontend working
#####graphController
`/static/js/app/app.js`
1. We provide a dummy states so that the graph could be drawn. Sometime `load_data` function doesnt get the data in time and our beloved D3 will be soup.
2. `load_data` loads all the states ( should limit it to some decent number), after its done, it gives goahead to the function linked to interval  by setting `start_interval=true`
3. Interval then calls the function every 10 seconds.
4. We watch the `$scope.stockData` for changes and call `redrawLineChart`
5. To make the app responsive, graph dimensions are linked to wrapper width. on window resize, the graph is removed and readded. [ Find anything wrong here ;) ]

#####stocksController
`/static/js/app/app.js`
very basic stuff, fetches the data every 10 seconds and and updates the table.

##### Styling
Very basic stuff, located at `static/css/main.css`

####### Known issues
* Writing the readme, I just realized that I could hard code the first state in index.html pulling it from the db instead of some dummy data in index.html
* Graph is redrawn on every resize even when its not necessary, like 1000 to 1200px. I factored that when I was using a different method, but forgot to include it in this.
* D3 loops through all data points every time. That should be improved. (Update: As a temporary fix, limited it to 10k data points )
