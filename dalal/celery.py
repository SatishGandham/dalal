from __future__ import absolute_import
from datetime import timedelta

import os

from celery import Celery

from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dalal.settings')

app = Celery('dalal')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERYBEAT_SCHEDULE={
        'update-stock-prices-every-10-seconds': {
            'task': 'stocks.tasks.update_stock_prices',
            'schedule': timedelta(seconds=10),
        },
    }
)
